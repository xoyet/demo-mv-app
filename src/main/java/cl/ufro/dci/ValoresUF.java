package cl.ufro.dci;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.Formatter;
import java.util.TreeMap;

public class ValoresUF {
    private TreeMap<LocalDate,Double> treeMapUF;
    private Document document;
    private final String[] meses = {"Enero","Febrero", "Marzo","Abril",
            "Mayo","Junio","Julio","Agosto",
            "Septiembre","Octubre","Noviembre","Diciembre"};

    public ValoresUF(String url) throws IOException {
        document = Jsoup.connect(url).get();
        treeMapUF = llenarListaById(2020);
    }

    private TreeMap<LocalDate,Double> llenarListaById(int año){
        String spanId, dia,mes;
        TreeMap<LocalDate,Double> preTreeMapUF = new TreeMap<>();
        LocalDate fecha = LocalDate.of(año,1,1);
        while(fecha.getYear() == año){
            Formatter formatter = new Formatter();
            /*  Se crean los componentes de las id de los span que contienen los valores.
                spanId: contiene la base de todas las ids de los span.
                dia: Número de dia con formato de dos cifras, utilizado como parte de la id.
                mes: Nombre en español del mes del año, utilizado como parte de la id. */
            spanId = "gr_ctl";
            dia = String.valueOf(formatter.format("%02d",(fecha.getDayOfMonth()+1)));
            mes = "_"+meses[fecha.getMonthValue()-1];
            Element spanUF = document.getElementById(spanId+dia+mes);
            if (spanUF != null) {
                if (!spanUF.html().isEmpty()) {
                    double valorUF = Double.parseDouble(spanUF.html().replace(".", "").replace(",", "."));
                    preTreeMapUF.put(fecha, valorUF);
                }
            }
            fecha = fecha.plusDays(1);
        }
        return preTreeMapUF;
    }
    public double buscaUF(int dia, int mes, int año) throws DateTimeException {
        LocalDate fechaBuscada = LocalDate.of(año,mes,dia);
        if(treeMapUF.containsKey(fechaBuscada)){
            return treeMapUF.get(fechaBuscada);
        }else{
            throw new DateTimeException("Registro no Encontrado");
        }
    }


}
