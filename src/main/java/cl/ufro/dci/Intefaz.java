package cl.ufro.dci;

import javax.swing.*;
import java.io.IOException;
import java.time.DateTimeException;

public class Intefaz {

    private final String url = "https://si3.bcentral.cl/indicadoressiete/secure/Serie.aspx?gcode=UF&param=RABmAFYAWQB3AGYAaQBuAEkALQAzADUAbgBNAGgAaAAkADUAVwBQAC4AbQBYADAARwBOAGUAYwBjACMAQQBaAHAARgBhAGcAUABTAGUAYwBsAEMAMQA0AE0AawBLAF8AdQBDACQASABzAG0AXwA2AHQAawBvAFcAZwBKAEwAegBzAF8AbgBMAHIAYgBDAC4ARQA3AFUAVwB4AFIAWQBhAEEAOABkAHkAZwAxAEEARAA=";
    private ValoresUF valoresUF;

    public Intefaz(){
        try {
            valoresUF = new ValoresUF(url);
        } catch (IOException ioException) {
            JOptionPane.showMessageDialog(null, ioException.getMessage(), "Error", JOptionPane.OK_OPTION);
        }
    }

    public void lineaAccion(){
        String fecha = JOptionPane.showInputDialog("Ingrese dia, mes y año (dd/mm/aaaa)");
        if(fecha!=null){
            String[] fechaArray = fecha.split("/");
            int dia = Integer.parseInt(fechaArray[0]),
                    mes = Integer.parseInt(fechaArray[1]),
                    año = Integer.parseInt(fechaArray[2]);
            try {
                JOptionPane.showMessageDialog(null,
                        "En la fecha "+dia+"/"+mes+"/"+año+" la UF valía: "+valoresUF.buscaUF(dia,mes,año));
            } catch (DateTimeException dateTimeException){
                JOptionPane.showConfirmDialog(null,"Fecha invalidad\n"+dateTimeException.getMessage(),
                        "Error",JOptionPane.DEFAULT_OPTION,JOptionPane.ERROR_MESSAGE);
            }
        }
        if (0==JOptionPane.showConfirmDialog(null,"¿Otra Busqueda?",
                "Selecciona una opción",JOptionPane.YES_NO_OPTION)){
            this.lineaAccion();
        };
    }



}
